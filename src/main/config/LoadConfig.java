package main.config;

import main.config.CommonEnums.DriverType;
import main.utils.FileUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class LoadConfig {

    private static Properties CONFIG = null;
    private static final String propertyFile = "config.properties";

    public static Properties loading_config_sys() throws IOException {
        FileInputStream fs = null;
        try {
            String propertyFilePath = FileUtils.relativePathToFullPath(propertyFile);
            fs = new FileInputStream(propertyFilePath);
            CONFIG = new Properties();
            CONFIG.load(fs);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return CONFIG;
    }

    public static String getSystemProperties(String propKey) {
        String propValue = "";
        try {
            if (CONFIG == null) {
                CONFIG = LoadConfig.loading_config_sys();
            }
            propValue = CONFIG.getProperty(propKey);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propValue;
    }

    public static String getUserName() {
        return getSystemProperties("user.name");
    }

    public static String getPassword() {
        return getSystemProperties("user.pass");
    }

    public static DriverType getBrowser() {
        String browserName = getSystemProperties("browserName");
        if (browserName.equalsIgnoreCase("chrome")) return CommonEnums.DriverType.CHROME;

        else if (browserName.equalsIgnoreCase("coccoc")) return CommonEnums.DriverType.COCCOC;
        else return CommonEnums.DriverType.FIREFOX;
    }

    public static String getBrowserPath() {
        return getSystemProperties("browserPath");
    }

    public static String getURL() {
        return getSystemProperties("url");
    }

    public static String getmode() {
        return getSystemProperties("mode");
    }

    public static String getLanguage() {
        String language = getSystemProperties("language");
        if (language == null) {
            language = "vietnam";
        }

        return language;
    }


}

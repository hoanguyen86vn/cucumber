package main.config;

public final class CommonConstants
{
	public static final class Driver
	{
		public static final String GECKO_DRIVER_PATH = "resource\\driver\\geckodriver.exe";
		public static final String CHROME_DRIVER_PATH = "resource\\driver\\chromedriver.exe";
	}

	public static final class Report
	{
		public static final String CONFIG_REPORT_PATH = "resource\\extent-config.xml";
	}

	public static final class Configuration
	{
		public static final int IMPLICITLY_WAIT = 60;
		public static final int TIME_TO_WAIT = 60;
	}

	public static final class EbxLog
	{
		public static final String KERNEL_LOG_PATH = "_ebx-eclipse\\ebx-run\\ebxLog\\kernel.log";
		public static final String DATASERVICES_LOG_PATH = "_ebx-eclipse\\ebx-run\\ebxLog\\dataServices.log";
	}

	public static final class EbxRepository
	{
		public static final String SCHEMAS_PATH = "_ebx-eclipse\\ebx-run\\ebxRepository\\schemas";
		public static final String H2_PATH = "_ebx-eclipse\\ebx-run\\ebxRepository\\h2\\repository.h2.db";
		public static final String H2_CHECK_PATH = "_ebx-eclipse\\ebx-run\\ebxRepository\\h2\\repository1";
		public static final String DATAMODEL_PATH = "_ebx-eclipse\\ebx-run\\ebxRepository\\archives\\dataModels";
	}

	public static final class H2FileDatabase
	{
		public static final String DB_DRIVER = "org.h2.Driver";
		public static final String DB_CONNECTION = "jdbc:h2:file:";
		public static final String DB_USER = "sa";
		public static final String DB_PASSWORD = "";
	}

	public static final class InputOutput
	{
		public static final String INPUT_PATH = "input";
		public static final String OUTPUT_PATH = "output";
		
	}

	public final class SOAPClient
	{
		public static final String SOAP_URL = "http://localhost:8080/ebx-dataservices/connector";

		public static final String USERNAME_TOKEN = "UsernameToken";
		public static final String USERNAME = "Username";
		public static final String PASSWORD = "Password";

		public static final String WS_SECURITY_NAMESPACE = "http://schemas.xmlsoap.org/ws/2002/04/secext";
		public static final String SECURITY = "Security";
		public static final String WSSE_PREFIX = "sec";

		public static final String WS_BODY_NAMESPACE = "urn:ebx-schemas:dataservices_1.0";
		public static final String COMMAND_PREFIX = "urn";
	}
	
	public static final class SQLDatabase
	{
		public static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
		public static final String DB_CONNECTION = "jdbc:oracle:thin:@10.225.3.14:1521:EBX";
		public static final String DB_USER = "ADDON_DE_2";
		public static final String DB_PASSWORD = "ADDON_DE_2";
	}

	

	public static final class Email
	{
		public static final String JBOSS_EMAIL_PROVIDER = "pop3";
		public static final String JBOSS_EMAIL_HOST = "webmail.ifisolution.com";
		public static final String JBOSS_EMAIL_ACCOUNT = "mdm_test01@ifisolution.com";
		public static final String JBOSS_EMAIL_PASSWORD = "idm12012";

	}

	
}
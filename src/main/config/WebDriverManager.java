package main.config;

import main.config.CommonEnums.DriverType;
import main.utils.FileUtils;
import main.utils.LogMessageUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public  class WebDriverManager {
	public static  String baseUrl;
	public static WebDriver webDriver;
	public static WebDriverWait webDriverWait;

	public static  void setUp() throws Exception {
		LogMessageUtils.info("Setting driver before running SC");
		setBaseUrl();
		setDriver();
		setDriverWait();
	}
	
	
	public static  void setBaseUrl() {
		baseUrl = LoadConfig.getURL();
		
	}


	public static  void setDriver() throws Exception {
		DriverType driverType = LoadConfig.getBrowser();
		switch (driverType) {
		case FIREFOX:
			webDriver = initFirefoxDriver();
			break;
		case CHROME:
			webDriver = initChromeDriver();
			break;
		
		case COCCOC:
			webDriver = initCoccocDriver();
			break;
		default:
			webDriver = initFirefoxDriver();
		}
	}

	public static  void setDriverWait() {
		webDriverWait = new WebDriverWait(webDriver, CommonConstants.Configuration.TIME_TO_WAIT);
	}

	public static  WebDriverWait getDriverWait() {
		return webDriverWait;
	}

	public static  WebDriver getDriver() {
		return webDriver;
	}

	public static  String getBaseUrl() {
		return baseUrl;
	}

	

	private static WebDriver initFirefoxDriver() throws Exception {
		LogMessageUtils.info("Launching Firefox browser...");
		
		String fullPath = FileUtils.relativePathToFullPath(CommonConstants.Driver.GECKO_DRIVER_PATH);
		System.setProperty("webdriver.firefox.marionette", fullPath);

		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);

		webDriver = new FirefoxDriver();
		webDriver.manage().timeouts().implicitlyWait(CommonConstants.Configuration.IMPLICITLY_WAIT, TimeUnit.SECONDS);

		webDriver.manage().window().maximize();
		return webDriver;
	}

	private static WebDriver initChromeDriver() throws Exception {
		LogMessageUtils.info("Launching Chrome browser...");
		
		String fullPath = FileUtils.relativePathToFullPath(CommonConstants.Driver.CHROME_DRIVER_PATH);
		System.setProperty("webdriver.chrome.driver", fullPath);

		webDriver = new ChromeDriver();
		webDriver.manage().timeouts().implicitlyWait(CommonConstants.Configuration.IMPLICITLY_WAIT, TimeUnit.SECONDS);

		webDriver.manage().window().maximize();
		return webDriver;
	}
	
	private static WebDriver initCoccocDriver() throws Exception {
		LogMessageUtils.info("Launching Coccoc browser...");
		
		String fullPath = FileUtils.relativePathToFullPath(CommonConstants.Driver.CHROME_DRIVER_PATH);
		System.setProperty("webdriver.chrome.driver", fullPath);
		
		String browserPath = LoadConfig.getBrowserPath();

		ChromeOptions options = new ChromeOptions();
		options.setBinary(browserPath);
		
		webDriver = new ChromeDriver(options);
		webDriver.manage().timeouts().implicitlyWait(CommonConstants.Configuration.IMPLICITLY_WAIT, TimeUnit.SECONDS);

		webDriver.manage().window().maximize();
		return webDriver;
	}
	
//	private static WebDriver initInternetExplorerDriver() throws Exception
//	{
//		LogMessageUtils.info("Launching IE browser...");
//		
//		String fullPath = FileUtils.relativePathToFullPath(CommonConstants.Driver.IE_DRIVER_PATH);
//		System.setProperty("webdriver.ie.driver", fullPath);
//		
//		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
//		capabilities.setCapability(
//			InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
//			true);
//		webDriver = new InternetExplorerDriver();
//		webDriver.manage().timeouts().implicitlyWait(CommonConstants.Configuration.IMPLICITLY_WAIT, TimeUnit.SECONDS);
//
//		webDriver.manage().window().maximize();
//		return webDriver;
//	}

//	public static void setUpWithOutput(String path) {
//		try {
//			String downloadPath = FileUtils.relativePathToFullPath(path);
//			File file = new File(downloadPath);
//			if (!file.exists()) {
//				file.mkdirs();
//			}
//			setBaseUrl();
//
//			String fullPath = FileUtils.relativePathToFullPath(CommonConstants.Driver.GECKO_DRIVER_PATH);
//			System.setProperty("webdriver.firefox.marionette", fullPath);
//
//			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
//			capabilities.setCapability("marionette", true);
//
//			FirefoxProfile profile = new FirefoxProfile();
//			profile.setPreference("browser.download.dir", downloadPath);
//			profile.setPreference("browser.download.folderList", 2);
//			profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
//					"application/force-download, application/octet-stream");
//
//			capabilities.setCapability(FirefoxDriver.PROFILE, profile);
//
//			webDriver = new FirefoxDriver(capabilities);
//
//			webDriver.manage().timeouts().implicitlyWait(CommonConstants.Configuration.IMPLICITLY_WAIT,
//					TimeUnit.SECONDS);
//			webDriverWait = new WebDriverWait(webDriver, CommonConstants.Configuration.TIME_TO_WAIT);
//			webDriver.manage().window().maximize();
//		} catch (Exception e) {
//			System.out.println(e);
//			fail("Cannot set-up the environment!");
//		}
//	}
}
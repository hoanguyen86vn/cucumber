package main.pages;


import main.config.WebDriverManager;
import main.utils.LogMessageUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import static org.junit.Assert.fail;
public  class BasePage
{
	public static final String DIV_MAIN_MENU  = "//*[text()='Bắt đầu với Quảng Cáo Cốc Cốc']";

	public  WebDriver webDriver;
	public  WebDriverWait webDriverWait;
	
	public  BasePage () {
		webDriver =  WebDriverManager.getDriver();
		webDriverWait =  WebDriverManager.getDriverWait();
	}
	
	
	public  WebElement getElement(String xPath) {
		return webDriver.findElement(By.xpath(xPath));
	}
	
	/*public  void closePopUp() {
		webDriver.switchTo().defaultContent();
		clickElement(CLOSE_POPUP);
	}

	public void closePopUpInFrame() {
		clickElement(CLOSE_POPUP_IN_FRAME);
		webDriver.switchTo().defaultContent();
		pause(1000);
	}*/

	public void pause(int milisecond) {
		try {
			Thread.sleep(milisecond);
		} catch (Exception e) {
			LogMessageUtils.info("Pause command with exception"+e);
		}
	}

	public void typeText(
			String xpath, String value) {
		LogMessageUtils.info("Type text into element: " + xpath);
		try {
			webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			getElement(xpath).clear();
			getElement(xpath).sendKeys(value);
		} catch (Exception e) {
			LogMessageUtils.error("Cannot type text into element " + xpath + ": " + e);
			fail("Cannot type text into element " + xpath + ": " + e);
		}
	}

	public void typeText(
			String xpath, int index, String value) {
		LogMessageUtils.info("Type text into element: " + xpath);
		List<WebElement> children = webDriver.findElements(By.xpath(xpath));
		WebElement element = children.get(index);
		try {
			webDriverWait.until(ExpectedConditions.visibilityOf(element));
			element.clear();
			element.sendKeys(value);
		} catch (Exception e) {
			LogMessageUtils.error("Cannot type text into element " + xpath + ": " + e);
			fail("Cannot type text into element " + xpath + ": " + e);
		}
	}

	public void clickElement(String xpath) {
		LogMessageUtils.info("Click into element: " + xpath);
		try {
			webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			getElement(xpath).click();
		} catch (Exception e) {
			LogMessageUtils.error("Cannot click into element " + xpath + ": " + e);
			fail("Cannot click into element " + xpath + ": " + e);
		}
	}
	
	public void clickElementAndWait(String xpath) {
		LogMessageUtils.info("Click into element: " + xpath);
		try {
			webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			getElement(xpath).submit();
		} catch (Exception e) {
			LogMessageUtils.error("Cannot click into element " + xpath + ": " + e);
			fail("Cannot click into element " + xpath + ": " + e);
		}
	}

	/*public void addForeignKey(
			String xpath, String value) {
		LogMessageUtils.info("Add a value of dropdown list : " + xpath);
		typeText(xpath, value);
		waitForVisible(ISS_RESULTS);
		clickElement(
				ISS_RESULTS + "//*[contains(text(),'" + value + "')]");
	}*/

	public void waitForInvisible(String xpath) {
		try {
			webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
		} catch (Exception e) {
			LogMessageUtils.error("Unexpected element display " + xpath + ": " + e);
			fail("Unexpected element display " + xpath + ": " + e);
		}
	}

	public void waitForVisible(String xpath) {
		try {
			webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		} catch (Exception e) {
			LogMessageUtils.error("Time out when finding element " + xpath + ": " + e);
			fail("Time out when finding element " + xpath + ": " + e);
		}
	}

	public void doubleClick(String xpath) {
		try {
			webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
			Actions action = new Actions(WebDriverManager.getDriver());
			action.moveToElement(getElement(xpath)).doubleClick().perform();
		} catch (Exception e) {
			LogMessageUtils.error("Cannot doube click into element " + xpath + ": " + e);
			fail("Cannot doube click into element " + xpath + ": " + e);
		}
	}

	public void selectByText(
			String xpath, String option) {
		try {
			webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			Select selectField = new Select(getElement(xpath));
			selectField.selectByVisibleText(option);
		} catch (Exception e) {
			LogMessageUtils.error("Cannot select value for element " + xpath + ": " + e);
			fail("Cannot select value for element " + xpath + ": " + e);
		}
		pause(1000);
	}

	public void selectOptionWithContainText(
			String xpath, String text) {
		try {
			Select selectField = new Select(getElement(xpath));
			List<WebElement> list = selectField.getOptions();
			for (WebElement option : list) {
				String fullText = option.getText();
				if (fullText.contains(text)) {
					selectField.selectByVisibleText(fullText);
					return;
				}
			}
		} catch (Exception e) {
			LogMessageUtils.error("Cannot select value for element " + xpath + ": " + e);
			fail("Cannot select value for element " + xpath + ": " + e);
		}
		pause(1000);
	}

	public void selectOptionByValue(
			String xpath, String option) {
		try {
			webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			Select selectField = new Select(getElement(xpath));
			selectField.selectByValue(option);
		} catch (Exception e) {
			LogMessageUtils.error("Cannot select value for element " + xpath + ": " + e);
			fail("Cannot select value for element " + xpath + ": " + e);
		}
		pause(1000);
	}

	/*public void selectPopUpFrame() {
		try {
			WebDriverManager.getDriverWait()
					.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(getElement(POPUP_FRAME)));
		} catch (Exception e) {
			LogMessageUtils.logERROR("Cannot select popup frame " +  ": " + e);
		}
		pause(3000);
	}*/

	public void acceptAlert() {
		try {
			webDriverWait.until(ExpectedConditions.alertIsPresent());
			webDriver.switchTo().alert().accept();
		} catch (Exception e) {
			LogMessageUtils.info("Accept alert with exception" + e);
		}
	}

	public void scrollBar(String xpath) {
		/*
		 * scroll bar down until that element appear on page.
		 */
		JavascriptExecutor je = (JavascriptExecutor) WebDriverManager.getDriver();
		WebElement element = getElement(xpath);
		je.executeScript("arguments[0].scrollIntoView(true);", element);
	waitForVisible(xpath);
	}

	public List<WebElement> listOption(String xpath) {
		WebElement element = getElement(xpath);
		Select s = new Select(element);
		List<WebElement> list = s.getOptions();
		return list;
	}

	public void pressEnter(String xpath) {
		try {
			getElement(xpath).sendKeys(Keys.ENTER);
		} catch (Exception e) {
			LogMessageUtils.info("Press Enter with exception" + e);
		}
	}

	public void ctrlC(String xpath) {
		WebElement element = getElement(xpath);
		element.click();
		element.sendKeys(Keys.CONTROL + "c");
	}

	public void ctrlV(String xpath) {
		WebElement element = getElement(xpath);
		element.click();
		element.sendKeys(Keys.CONTROL + "v");
	}

	
	
//	public static void checkMsgBox( String message)
//	{
//		CommonUtils.pause(1000);
//		CommonUtils.waitForVisible( "//*[@id='ebx_DisplayMessageBox']");
//		CommonUtils.clickElement( "//*[@id='ebx_DisplayMessageBox']");
//		CommonUtils.waitForVisible( "//*[contains(text(),'" + message + "')]");
//		CommonUtils.clickElement( "//*[@id='ebx_MessageBox_c']//*[text()='Close']");
//	}
//	
//	public void clickHelp(  String xpath)
//	{
//		String winHandleBefore = webDriver.getWindowHandle();
//		/*
//		 *  Perform the click operation that opens new window
//		 */
//		CommonUtils.clickElement( "//*[@id='ebx_ContextualDoc']");
//		/*
//		 *  Switch to new window opened
//		 */
//		for (String winHandle : webDriver.getWindowHandles())
//		{
//			webDriver.switchTo().window(winHandle);
//		}
//		/*
//		 *  Perform the actions on new window
//		 */
//		CommonUtils.waitForVisible( xpath);
//		/*
//		 *  Close the new window, if that window no more required
//		 */
//		webDriver.close();
//		/*
//		 *  Switch back to original browser (first window)
//		 */
//		webDriver.switchTo().window(winHandleBefore);
//	}
} 
package main.pages;

import main.config.LoadConfig;
import main.config.WebDriverManager;
import main.utils.LogMessageUtils;

import static org.junit.Assert.fail;

public class LoginPage extends BasePage {

    public static String TXT_USER_LOGIN = "//input[@placeholder='Nhập email']";
    public static String TXT_PASSWORD_LOGIN = "//input[@placeholder='Nhập mật khẩu']";
    public static String BT_LOGIN = "//button[text()='Đăng nhập']";

    public static String L_FORGET_PASSWORD = "//a[text()='Quên mật khẩu?']";
    public static String L_CREATE_ACCOUNT = "//a[contains(text(),'Đăng ký ngay')]";
    public static String BT_LOGIN_FECEBOOK = "//*[text()='Facebook']";
    public static String BT_LOGIN_GOOGLE = "//*[text()='Google']";

    public static String P_FORGET_PASSWORD = "//*[text()='Khôi Phục Mật Khẩu']";
    public static String P_CREATE_ACCOUNT = "//*[contains(text(),'Tôi đồng ý với')]";
    public static String P_LOGIN_FACEBOOK = "//span[contains(text(),'Log in to Facebook')]";
    public static String P_LOGIN_GOOGLE = "//*[text()='Đăng nhập bằng Google']";

    public void accessURL(String url) {
        try {
            WebDriverManager.getDriver().get(url);
        } catch (Exception e) {
            LogMessageUtils.error("Fail to open URL: " + url + " : " + e);
            fail("Fail to open URL: " + url + " : " + e);
        }
    }

    public void inputAdminAccount() {
        typeText(TXT_USER_LOGIN, LoadConfig.getUserName());
        typeText(TXT_PASSWORD_LOGIN, LoadConfig.getPassword());
    }

    public void inputAccount(String user, String pass) {
        typeText(TXT_USER_LOGIN, user);
        typeText(TXT_PASSWORD_LOGIN, pass);
    }

    public void clickLogin() {
        clickElement(BT_LOGIN);
    }

    public void checkErrorMsg(String msg) {
        String ERROR_MSG = "//*[text()='" + msg + "']";
        waitForVisible(ERROR_MSG);
    }

    public void checkLoginSuccess() {
        waitForVisible(DIV_MAIN_MENU);
    }

    public void clickForgetPassword() {
        clickElement(L_FORGET_PASSWORD);
    }
    public void clickCreateAccount() {
        clickElement(L_CREATE_ACCOUNT);
    }
    public void clickLoginFacebook() {
        clickElement(BT_LOGIN_FECEBOOK);
    }
    public void clickLoginGoogle() {
        clickElement(BT_LOGIN_GOOGLE);
    }

    public void checkRedirectForgotPasswordPage() {
        waitForVisible(P_FORGET_PASSWORD);
    }
    public void checkRedirectCreateAccountPage() {
        waitForVisible(P_CREATE_ACCOUNT);
    }
    public void checkRedirectLoginFacebookPage() {
        waitForVisible(P_LOGIN_FACEBOOK);
    }
    public void checkRedirectLoginGooglePage() {
        waitForVisible(P_LOGIN_GOOGLE);
    }
}
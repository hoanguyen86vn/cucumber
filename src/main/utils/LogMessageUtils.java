package main.utils;
import org.apache.log4j.Logger;

public class LogMessageUtils {
	public static Logger logs = Logger.getLogger(LogMessageUtils.class);

	private LogMessageUtils()
	{
	}


	/*public static void logERROR(String message) {
		System.out.println("----> ERROR <----- : " + message);

		fail("----> ERROR <----- :" + message);
	}

	*/public static void logINFO(String message) {
		System.out.println("----> INFORMATION <----- : " + message);
	}



	public static void debug(String msg)
	{
		logs.debug(">> debug: | " + msg);
		System.out.println(">> debug: | " + msg);
	}

	public static void info(String msg)
	{
		logs.info(">> info: | " + msg);
		System.out.println(">> info: | " + msg);
	}

	public static void warn(String msg)
	{
		logs.warn(">> warn: | " + msg);
		System.out.println(">> warn: |" + msg);
	}

	public static void error(String msg)
	{
		logs.error(">> error: | " + msg);
		System.out.println(">> error: |" + msg);
	}
}

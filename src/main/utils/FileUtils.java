package main.utils;

import main.config.CommonConstants;
import org.apache.commons.io.input.ReversedLinesFileReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static org.junit.Assert.fail;

public class FileUtils {
	public static String relativePathToFullPath(String filePath) throws IOException {
		File file = new File(filePath);
		String absolutePath = file.getCanonicalFile().getAbsolutePath();
		// It's for Linux
		// if (absolutePath.indexOf("/") == -1)
		// {
		// absolutePath = absolutePath.replaceAll("[\\\\]", "\\\\\\\\");
		// }
		/*if (absolutePath.contains("ant")) {
			absolutePath = absolutePath.replace("ant\\", "");
		}
		if (absolutePath.contains("launches")) {
			absolutePath = absolutePath.replace("launches\\", "");
		}
		if (absolutePath.contains("_ebx-eclipse")) {
			if (absolutePath.contains("addon-tests-ebx")) {
				absolutePath = absolutePath.replace("addon-tests-ebx\\", "");
			}
		}*/

		LogMessageUtils.info("Full path of " + filePath + " is " + absolutePath);
		return absolutePath;
	}

	public static void unzipFile(String source, String dest) {
		ZipInputStream zipIs = null;
		try {
			String sourcePath = FileUtils.relativePathToFullPath(source);
			String destPath = FileUtils.relativePathToFullPath(dest);
			byte[] buffer = new byte[1024];
			zipIs = new ZipInputStream(new FileInputStream(sourcePath));
			ZipEntry entry = null;
			while ((entry = zipIs.getNextEntry()) != null) {
				String entryName = entry.getName();
				String outFileName = destPath + File.separator + entryName;
				if (entry.isDirectory()) {
					new File(outFileName).mkdirs();
				} else {
					FileOutputStream fos = new FileOutputStream(outFileName);
					int len;
					while ((len = zipIs.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}
					fos.close();
				}
			}
		} catch (Exception e) {
			LogMessageUtils.error("Fail to unzip file with exception: " + e);
			fail("Fail to unzip file with exception: " + e);
		} finally {
			try {
				if (zipIs != null)
					zipIs.close();
			} catch (Exception e) {
				LogMessageUtils.error("Fail to unzip file with exception: " + e);
				fail("Fail to unzip file with exception: " + e);
			}
		}
	}

	public static void deleteFile(String filePath) {
		try {
			String fullpath = FileUtils.relativePathToFullPath(filePath);
			File file = new File(fullpath);
			if (file.exists()) {
				file.delete();
			}
		} catch (Exception e) {
			LogMessageUtils.error("Fail to delete file with exception: " + e);
			fail("Fail to delete file with exception: " + e);
		}
	}

	public static void copyFileUsingFileChannels(String source, String dest) {
		/*
		 * copy big file
		 */
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			File sourcePath = new File(FileUtils.relativePathToFullPath(source));
			File destPath = new File(FileUtils.relativePathToFullPath(dest));
			fis = new FileInputStream(sourcePath);
			fos = new FileOutputStream(destPath);
			inputChannel = fis.getChannel();
			outputChannel = fos.getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} catch (IOException e) {
			LogMessageUtils.error("Fail to copy file with exception: " + e);
			fail("Fail to copy file with exception: " + e);
		} finally {
			try {
				if (inputChannel != null)
					inputChannel.close();
				if (fis != null)
					fis.close();
				if (outputChannel != null)
					outputChannel.close();
				if (fos != null)
					fos.close();
			} catch (IOException e) {
				LogMessageUtils.error("Fail to copy file with exception: " + e);
				fail("Fail to copy file with exception: " + e);
			}
		}
	}

	public static void copyFileUsingFileStreams(String source, String dest) {
		InputStream input = null;
		OutputStream output = null;
		try {
			File sourcePath = new File(FileUtils.relativePathToFullPath(source));
			File destPath = new File(FileUtils.relativePathToFullPath(dest));
			input = new FileInputStream(sourcePath);
			output = new FileOutputStream(destPath);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
		} catch (IOException e) {
			LogMessageUtils.error("Fail to copy file with exception: " + e);
			fail("Fail to copy file with exception: " + e);

		} finally {
			try {
				if (input != null)
					input.close();
				if (output != null)
					output.close();
			} catch (IOException e) {
				LogMessageUtils.error("Fail to copy file with exception: " + e);
				fail("Fail to copy file with exception: " + e);

			}
		}
	}

	public static boolean checkLog(String fileName, String error) {
		/*
		 * check 100 lines in log and from the end of file
		 */
		ReversedLinesFileReader fReader = null;
		try {
			String pathLog = FileUtils.relativePathToFullPath(fileName);
			fReader = new ReversedLinesFileReader(new File(pathLog));
			String line;
			int number = 0;
			while (((line = fReader.readLine()) != null) && (number < 100)) {
				if (line.contains(error)) {
					return true;
				}
				number++;
			}
		} catch (IOException e) {
			LogMessageUtils.error("Fail to read file with exception: " + e);
			fail("Fail to read file with exception: " + e);
		} finally {
			try {
				if (fReader != null) {
					fReader.close();
				}
			} catch (IOException e) {
				LogMessageUtils.error("Fail to read file with exception: " + e);
				fail("Fail to read file with exception: " + e);
			}
		}
		return false;
	}

	public static boolean checkFile(String fileName, String str) {
		FileReader fReader = null;
		BufferedReader bReader = null;
		try {
			String pathLog = FileUtils.relativePathToFullPath(fileName);
			fReader = new FileReader(pathLog);
			bReader = new BufferedReader(fReader);
			String line;
			while ((line = bReader.readLine()) != null) {
				if (line.contains(str)) {
					return true;
				}
			}
		} catch (IOException e) {
			LogMessageUtils.error("Fail to read file with exception: " + e);
			fail("Fail to read file with exception: " + e);
		} finally {
			try {
				if (bReader != null) {
					bReader.close();
				}
				if (fReader != null) {
					fReader.close();
				}
			} catch (IOException e) {
				LogMessageUtils.error("Fail to read file with exception: " + e);
				fail("Fail to read file with exception: " + e);

			}
		}
		return false;
	}

	public static boolean checkExceptionInKernelLog(String exception) {
		return checkLog(CommonConstants.EbxLog.KERNEL_LOG_PATH, exception);
	}

	public static void removeNodeInXMLfile(String file, String xpath) {
		/*
		 * handle XML file
		 */
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			String fullPath = FileUtils.relativePathToFullPath(file);
			Document doc = factory.newDocumentBuilder().parse(new File(fullPath));
			/*
			 * find element by xpath
			 */
			XPath xp = XPathFactory.newInstance().newXPath();
			Element element = (Element) xp.evaluate(xpath, doc, XPathConstants.NODE);
			/*
			 * remove node
			 */
			element.getParentNode().removeChild(element);
			/*
			 * save file
			 */
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			t.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(fullPath)));
		} catch (Exception e) {
			LogMessageUtils.error("Fail to update XML file with exception: " + e);
			fail("Fail to update XML file with exception: " + e);
		}
	}

	public static void updateAttributeInXMLfile(String file, String xpath, String attribute, String newValue) {
		/*
		 * handle XML file
		 */
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			String fullPath = FileUtils.relativePathToFullPath(file);
			Document doc = factory.newDocumentBuilder().parse(new File(fullPath));
			/*
			 * find element by xpath
			 */
			XPath xp = XPathFactory.newInstance().newXPath();
			Element element = (Element) xp.evaluate(xpath, doc, XPathConstants.NODE);
			/*
			 * update attribute
			 */
			if ("text".equals(attribute)) {
				element.setTextContent(newValue);
			} else {
				element.setAttribute(attribute, newValue);
			}
			/*
			 * save file
			 */
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			t.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(fullPath)));
		} catch (Exception e) {
			LogMessageUtils.error("Fail to update XML file with exception: " + e);
			fail("Fail to update XML file with exception: " + e);
		}
	}

}

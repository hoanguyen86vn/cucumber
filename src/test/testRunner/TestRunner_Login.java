package test.testRunner;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src\\test\\features\\login\\login.feature", glue = {

        "test.stepDefinition"}, monochrome = true, strict = true, dryRun = false
       ,plugin = {"com.cucumber.listener.ExtentCucumberFormatter:output/reports/report_logIn.html"}

)

public class TestRunner_Login {

    @AfterClass
    public static void writeExtentReport() {

        Reporter.loadXMLConfig(new File("resource\\extent-config.xml"));
        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Selenium", "3.11.0");
        Reporter.setSystemInfo("Java Version", "1.8.0_171");

    }

}
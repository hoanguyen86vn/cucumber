Feature: Login
  Data test:
  1. The account already exists in the system
  |user|password|
  |hoanguyen86.vn@gmail.com|hoamai12|
  2. The account does not exist in the system
  |user|password|
  |nthoa123@gmail.com|hoamai123|

  Scenario: [TC_L01_10]-User login successfully
    Given User is in the login page
    When User enters the account information
      | username                 | password |
      | hoanguyen86.vn@gmail.com | hoamai12 |
    Then User login successfully

  Scenario Outline: [TC_L02_03]-User login unsuccessfully because of lacking user/password information
    Given User is in the login page
    When User enters the account information
      | username | password |
      | <user>   | <pass>   |
    Then User see the error message "<error>"
    Examples:
      | user                     | pass     | error                         |
      | hoanguyen86.vn@gmail.com |          | Bắt buộc phải nhập "Mật khẩu" |
      |                          | hoamai12 | Bắt buộc phải nhập "Email"    |

  Scenario Outline: [TC_L04_05_06]-User login unsuccessfully because of invalid information
    Given User is in the login page
    When User enters the account information
      | username | password |
      | <user>   | <pass>   |
    Then User see the error message "<error>"
    Examples:
      | user                     | pass      | error                                                                   |
      | nthoa                    | hoamai    | Email bạn nhập không hợp lệ. Vui lòng thử lại (Ví dụ: abc@example.com). |
      | nthoa123@gmail.com       | hoamai123 | Email hoặc mật khẩu không đúng!                                         |
      | hoanguyen86.vn@gmail.com | abc       | Sai mật khẩu                                                            |

  Scenario: [TC_L13]-User is redirected to Forgot password page when clicking on "Quên mật khẩu" link
    Given User is in the login page
    When User click on Quên mật khẩu link
    Then User is redirected to Forgot password page

  Scenario: [TC_L14]-User is redirected to Create an account page when clicking on "Đăng ký ngay" link
    Given User is in the login page
    When User click on Đăng ký ngay link
    Then User is redirected to Create an account page

  Scenario: [TC_L15]-User is redirected to Choose account facebook page when clicking on "Facebook" link
    Given User is in the login page
    When User click on Facebook link
    Then User is redirected to Choose account facebook page

  Scenario: [TC_L16]-User is redirected to Choose account google page when clicking on "Google" link
    Given User is in the login page
    When User click on Google link
    Then User is redirected to Choose account google page
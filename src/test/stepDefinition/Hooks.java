package test.stepDefinition;
import com.cucumber.listener.Reporter;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import main.config.WebDriverManager;
import main.utils.LogMessageUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
public class Hooks extends WebDriverManager{

	@Before
	public void setUp(Scenario scenario) throws Exception {
		DOMConfigurator.configure("log4j.xml");
		super.setUp();
		LogMessageUtils.info("TEST CASE ----- " + scenario.getName() + " ---- STARTED");
	}

	@After
	public void afterSC(  Scenario scenario) throws Exception {
		String featureName = scenario.getId().split(";")[0];
		String scenarioName = scenario.getName().replace(" ", "-");
		if (scenario.isFailed())
		takeScreenShot("output\\screenshot\\" + featureName +"\\"+ scenarioName + ".png");
		LogMessageUtils.info("TEST CASE -----" + scenario.getName() + "---- FINISHED");
		webDriver.quit();
	}
	
	public void takeScreenShot( String fileWithPath) throws Exception {
		File scrFile = ((TakesScreenshot) WebDriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
		File DestFile = new File(fileWithPath);
		FileUtils.copyFile(scrFile, DestFile);
	}
}
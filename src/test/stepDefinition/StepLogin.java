package test.stepDefinition;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.config.WebDriverManager;
import main.pages.LoginPage;
import main.pages.PageObjectManager;
import main.utils.LogMessageUtils;

import java.util.List;
import java.util.Map;

public class StepLogin {

    PageObjectManager pageObjectManager = new PageObjectManager();
    LoginPage logIn = pageObjectManager.getLoginPage();

    @Given("^User is in the login page$")
    public void userIsInTheLoginPage() {
        logIn.accessURL(WebDriverManager.getBaseUrl());
    }

    @When("^User enters the account information$")
    public void userEntersTheAccountInformation(DataTable userData) {
        List<Map<String, String>> data = userData.asMaps(String.class, String.class);
        String user = data.get(0).get("username");
        String pass = data.get(0).get("password");
        logIn.inputAccount(user, pass);
        logIn.clickLogin();
    }

    @Then("^User login successfully$")
    public void userLoginSuccessfully() {
        logIn.checkLoginSuccess();
    }

    @Then("^User see the error message \"([^\"]*)\"$")
    public void userSeeTheErrorMessage(String arg0) {
        logIn.checkErrorMsg(arg0);
    }

    @Then("^User see the error message \"([^\"]*)\"Mật khẩu\"([^\"]*)\"$")
    public void user_see_the_error_message_Mật_khẩu(String arg1, String arg2) {
        logIn.checkErrorMsg(arg1 + "\"Mật khẩu\"");
    }

    @Then("^User see the error message \"([^\"]*)\"Email\"([^\"]*)\"$")
    public void user_see_the_error_message_Email(String arg1, String arg2) {
        logIn.checkErrorMsg(arg1 + "\"Email\"");
    }

    @When("^User click on Quên mật khẩu link$")
    public void userClickOnQuênMậtKhẩuLink() {
        logIn.clickForgetPassword();
    }

    @Then("^User is redirected to Forgot password page$")
    public void userIsRedirectedToForgotPasswordPage() {
        logIn.checkRedirectForgotPasswordPage();
    }

    @When("^User click on Đăng ký ngay link$")
    public void userClickOnĐăngKýNgayLink() {
        logIn.clickCreateAccount();
    }

    @Then("^User is redirected to Create an account page$")
    public void userIsRedirectedToCreateAnAccountPage() {
        logIn.checkRedirectCreateAccountPage();
    }

    @When("^User click on Facebook link$")
    public void userClickOnFacebookLink() {
        logIn.clickLoginFacebook();
    }

    @Then("^User is redirected to Choose account facebook page$")
    public void userIsRedirectedToChooseAccountFacebookPage() {
        logIn.checkRedirectLoginFacebookPage();
    }

    @When("^User click on Google link$")
    public void userClickOnGoogleLink() {
        logIn.clickLoginGoogle();
    }

    @Then("^User is redirected to Choose account google page$")
    public void userIsRedirectedToChooseAccountGooglePage() {
        logIn.checkRedirectLoginGooglePage();
        }
}
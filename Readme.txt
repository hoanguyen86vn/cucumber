This is a simple auto-test project using Selenium webdriver with POM + Cucumber BDD, Ant build tool
1. Tools and version
- JDK 1.8.0_171
- Ant 1.9.14
- Jenkins 2.177
- Cucumber for Java 181.5540.7, Gherkin 181.5540.7 (plug-in for IntelliJ IDE)
- Other tools in /resource/lib folder

2. How to run testing
- The simplest way: run the file \src\test\testRunner\TestRunner_Login.java (or you can run target in Ant build file)
- The default browser is Chrome, changing the browser by updating parameter in config.properties file
- Report, Log file and the screenshots (only steps fail) are in /output folder
Note: If test not run, to be sure that you added library files (in /resource/lib folder) into the project fully.

3. Intergrate with CI
 After installing, you configure Jenkins: 
- Manage Jenkins \ Global Tool Configuration
Setup JAVA_HOME and ANT_HOME are the install folder in the system
- Create job free-style project
In job configuration, Add a new Build step and select Invoke Ant, 
Select the Ant version which we have already configured and Ant build file
- In the workspace directory, put source code into here 
(this is the simplest way. Better if you push source code into git (or cvs, svn), then you can configure Source code manager)
- Go to the job index page and click Build Now, you can refer to the console output and check if there is any errors on the build script.